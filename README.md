# BuildStream examples

You will find BuildStream project examples here that try to help
getting to grips with using BuildStream - this serves as an aid to our
[documentation](https://buildstream.gitlab.io/buildstream/).

If you are stuck, don't be scared to ask on our [irc
channel](irc://irc.gnome.org/buildstream).

Contributions are, of course, welcome!
